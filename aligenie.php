<?php
	class AligenieIAP
	{
		public $utterance = "";
		public $request_city = "";
		public $skill_name = "";
		public $intent_name = "";
		public $slot_location = "";
		public $slot_date = "";

		function __construct($post_data) {
			$json_obj = json_decode($post_data, true);

			$this->utterance = $json_obj['utterance'];
			$this->skill_name = $json_obj['skillName'];
			$this->intent_name = $json_obj['intentName'];
			$this->request_city = @$json_obj['requestData']['city'];

			$slots = @$json_obj['slotEntities'];

			if (!empty($slots)) {
				foreach ($slots as $slot) {
					$intentParameterName = $slot['intentParameterName'];
					
					if ($this->startsWith($intentParameterName, "sys.location")) {
						$this->slot_location = $slot['slotValue'];
					} else if ($this->startsWith($intentParameterName, "sys.date")) {
						$this->slot_date = json_decode($slot['slotValue'], true)['iDateString'];
					}
				}
			}
		}

		public function toString() {
			return "Aligenie IAP request object:\n" .
				"\tutterance: $this->utterance\n" .
				"\trequest_city: $this->request_city\n" .
				"\tskill_name: $this->skill_name\n" .
				"\tintent_name: $this->intent_name\n" .
				"\tslot_location: $this->slot_location\n" .
				"\tslot_date: $this->slot_date\n";
		}

		private function startsWith($haystack, $needle) {
			$length = strlen($needle);
			return (substr($haystack, 0, $length) === $needle);
		}
	}
?>