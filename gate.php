<?php
	header('Content-Type: application/json');
	require_once('aligenie.php');

	$result = array(
		"returnCode" => "0",
		"returnErrorSolution" => "",
		"returnMessage" => "",
		"returnValue" => array(
			"reply" => "",
			"resultType" => "RESULT",
			"executeCode" => "SUCCESS",
			"msgInfo" => ""
		)
	);

	$post_str = file_get_contents("php://input");
	// file_put_contents("gate.json", $post_str);

	$aligenie = new AligenieIAP($post_str);
	// file_put_contents("debug", $aligenie->toString());

	switch ($aligenie->intent_name) {
		case "weather":
			$city = empty($aligenie->slot_location) ? empty($aligenie->request_city) ? "无" : $aligenie->request_city : $aligenie->slot_location;
			$date = empty($aligenie->slot_date) ? date("Y-m-d") : $aligenie->slot_date;
			$result['returnValue']['reply'] = "查询内容：天气；查询城市：$city ；查询日期：$date";

			break;
		default:
			$result['returnValue']['reply'] = "未查询到相关接口";
	}

	echo json_encode($result);
?>