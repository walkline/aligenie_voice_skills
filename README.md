<h1 align="center">天猫精灵语音技能</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

### 项目介绍

通过天猫精灵语音技能实现用对话方式进行自定义查询并使用语音反馈查询结果

> Python 版本的代码请访问 [有心大叔的项目](https://github.com/youxinweizhi/aligenie_skill)

### 前提条件

* [x] 有自己空间或者服务器
* [ ] 没有域名可以用IP地址代替
* [x] 开发者账号要绑定一台天猫精灵，用于真机测试

### 配置说明

1. 打开 [Aligenie 智能应用](https://iap.aligenie.com/home) 页面，登录你的开发者账号，点击 `技能`

	![](images/01.png)

2. 在`语音技能`页面点击`创建语音技能`按钮

	![](images/02.png)

3. 在`创建技能`页面填写相关信息，然后点击`确认创建`按钮

	> `调用词`：在唤醒天猫精灵后用于进入技能场景，尽量简短不重复，此处的唤醒词为`小助手`

	![](images/03.png)

4. 进入`语音交互模型创建`选项卡，点击`意图`、`创建意图`

	![](images/04.png)

5. 在`意图信息`页面填写相关信息，然后点击`提交`按钮
	> `意图标识`：用于在后端鉴别不同意图并实现相应的查询，此处意图标识为`weather`

	![](images/05.png)

6. 点击刚刚新建的意图右侧的`编辑`按钮

	![](images/06.png)

7. 找到`单轮对话表达`，在`例句`后边输入`天津明天天气`，按下`回车键`

	![](images/07.png)

8. 用鼠标拖选`天津`，在弹出的窗口中点击`引用系统实体`

	![](images/08.png)

9. `选取对应实体`下拉列表选择`城市 (sys.location)`，然后点击`确认`按钮

	![](images/09.png)

10. 用同样的方法拖选`明天`，实体选择`日期 (sys.date)`，最终结果如下图所示，然后点击`提交`按钮

	![](images/10.png)

11. 点击`回复逻辑`，展开`默认逻辑WEBHOOK`列表，可以看到我们刚刚新建的`意图标识 weather`，点击右侧的`详情`按钮

	![](images/11.png)

12. 将项目中的`*.php`文件上传到你的网站根目录下的`/aligenie`文件夹

	> 此处的文件夹并不限制非`/aligenie`不可，可以选用其它

13. 点击`编辑`按钮，`URL`地址为`gate.php`文件的完整地址

14. 点击`下载认证文件`，将下载来的文件上传到你的网站根目录下的`/aligenie`文件夹，然后点击`提交`按钮，如果操作无误的话会提示`保存成功`

	> 此处的`认证文件`必须保存到`/aligenie`文件夹，否则会认证失败

	![](images/12.png)

### 开始测试

#### 在线测试

进入`测试`选项卡，点`在线测试`，输入命令语句`小助手，明天北京天气`并回车观察结果，回复内容如图所示即为响应正确

![](images/13.png)

#### 真机测试

* 在`测试`选项卡点击`真机测试`，打开测试按钮

* 唤醒天猫精灵，然后说出命令语句`小助手，明天北京天气`，无误的话天猫精灵会语音回复`查询内容：天气；查询城市：北京 ；查询日期：2020-07-18`

### 合作交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
    * 走线物联：163271910
    * 扇贝物联：31324057

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>